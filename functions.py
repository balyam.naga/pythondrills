import math

n = input("enter the integer")

def isprime(item):
    is_prime = True

    if item%2 == 0:
     return False
    for i in range(2,math.ceil(item ** 0.5)+1):
        if(item%i == 0 and item != i):
            is_prime = False

    return is_prime

def primes(n):
 primes_list = []
 if n<2:
     return []
 if n==2:
     primes_list.append(2)

 for q in range(2, int(n)):
    if isprime(q) == True:
      primes_list.append(q)
 return primes_list

another_list = primes(n)
print("printing prime list",another_list)