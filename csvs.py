import csv
#Reading
#with open(filename, read) as fileName <- referred
#then create a reader based on the module csv and the referred name
with open('people.csv', 'r') as csvFile:
    reader1 = csv.reader(csvFile)
    for row in reader1:
        print(row)

csvFile.close()

#writing

def square(n):
    return n*n

def squares(n):
    rows = []
    for i in range(1,n+1):
     rows.append(i)
     rows.append(square(i))
    return rows

def squarecsv(n,file):
    with open(file, 'w') as squaredfile:
     writer1 = csv.writer(squaredfile)
     writer1.writerow(squares(10))
     squaredfile.close()

squarecsv(10,"csvfile.csv")