#break and continue

import math

def isprime(item):
    is_prime = True

    if item%2 == 0:
     return False
    for i in range(2,math.ceil(item ** 0.5)+1):
        if(item%i == 0 and item != i):
            is_prime = False

    return is_prime

for q in range(2, 102):
        #range taken as 102 just to use break
        if q == 2:
         print(q)
        if q >=100:
         break
        if isprime(q) == True:
         print(q)
