#all the elements in the given list
z = [1, 2, 3, 4]
print("all the elements in the given list")
for item in z:
    print(item)

#enumerate
obj = list(enumerate(z))
print("enumerating-->")
print(obj)

#10 to 0 in descending order using range and while
print("using range ")
for item in range(0,11):
    print(10-item)
item = 10
print("using while")
while(item>=0):
    print(item)
    item=item-1