class Point:

    def __init__(self,x,y):
        self.x = x
        self.y = y

    def draw(self):
        print(self.x, self.y)

    def distance(self,another_point):
        a = another_point.x - self.x
        b = another_point.y - self.y
        a = a*a
        b = b*b
        print((a+b)**0.5)

point1 = Point(10,20)
point1.draw()
point2 = Point(20,200)
point1.distance(point2)

