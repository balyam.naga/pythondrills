
#arbitrary function


def sum_n_numbers(*num):
    return sum(num)

i = sum_n_numbers(1, 2, 3, 4, 5, 6)
print(i)


#keyworded arbitrary function

def absolute_sum_n_numbers(*num, **keywords):
    result = 0
    if "absolute" in keywords.keys():
        if keywords["absolute"] == True:
            result = sum(num)
            return abs(result)


    return sum(result)

print(absolute_sum_n_numbers(2, 2, 3, 6, absolute = True))
