#n_digit_prime default value = 2
import math



a = input("enter the a")
def isprime(item):
    is_prime = True

    if item%2 == 0:
     return False
    for i in range(2,math.ceil(item ** 0.5)+1):
        if(item%i == 0 and item != i):
            is_prime = False

    return is_prime

def primes_default(a=2):
 p = 10
 end = p**int(a)
 startindex = int(a)-1
 start = p**startindex
 primes_list = []
 if int(a) < 2:
  primes_list.append(2)
 if startindex == 0:
     start = 3

 for q in range(start, end):
    if isprime(q) == True:
      primes_list.append(q)

 return primes_list

primes_default_list = primes_default(a)
print(primes_default_list)


#call wirth argument
primes_default_list = primes_default(a=2)
print(primes_default_list)